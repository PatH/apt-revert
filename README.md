apt-revert allows exactly that: reverting an apt call.

apt-revert does not have to be installed when the regrettable apt call (like
an update) was executed. You can install apt-revert after-the-fact and just
run it. For installation it is currently enough to just copy the file
apt-revert somewhere.

To call it, execute ./apt-revert (it must be executable, of course) as a
normal user.
Alternatively, you can also execute "julia apt-revert".
It depends on julia >= 1.4 and recommends devscripts (due to included debsnap).
Just install them via apt (don't be afraid, you won't need to revert this
installation immediately afterwards, as you can selectively revert a specific
apt call) and you should be ready to go on Debian Bullseye or newer.
Additionally, if julia < 1.6 is used, the Downloads package needs to be
installed within julia:
using Pkg; Pkg.add("Downloads")

The last-working package version after an upgrade of some packages is no
longer in one of the typical distributions (i.e. stable, testing, unstable
or experimental)? No problem, apt-revert uses debsnap to fetch the right
version from https://snapshot.debian.org or downloads Ubuntu packages from
https://launchpad.net.

apt-revert is normally called as a normal user. After collecting the necessary
deb files it calles apt with sudo, asking for root permission if necessary.
Therefore, apt-revert never gets the root password to not increase the attack
surface.

apt-revert is copyright 2020, 2021, 2022 Patrick Häcker.

License: GPLv2+

![Example call of apt-revert](screenshots/example_call.png?raw=true "Example call of apt-revert, selecting a change done by apt to get information about it and then starting to revert it.")